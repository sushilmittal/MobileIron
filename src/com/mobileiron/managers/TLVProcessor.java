package com.mobileiron.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.mobileiron.data.TLVDataType;
import com.mobileiron.data.TLVProcessedData;
import com.mobileiron.data.TLVProcessedRawData;
import com.mobileiron.utility.StringUtility;

/**
 * 
 * @author sushil
 *
 */
public class TLVProcessor {
	private static final String ROW_DATA_SEP = "-";
	public static BlockingQueue<String> rowDataToProcessQueue = new LinkedBlockingQueue<String>();

	static {
		startProcessingTLVData();
	}

	public static void addForProcessing(String tlvData) {
		rowDataToProcessQueue.add(tlvData);
	}

	public static void startProcessingTLVData() {
		Thread tlvDataProcessingThread = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						String tlvRawData = rowDataToProcessQueue.take();
						List<TLVProcessedRawData> processedRawDatas = getProcessedRawData(tlvRawData);

						for (TLVProcessedRawData processedRawData : processedRawDatas) {
							TLVProcessedData processedData = processedRawData
									.getProcessedData();
							System.out
									.println(processedData.getFormattedData());
						}
					} catch (InterruptedException e) {
						System.out.println("Thread Interupted exception " + e);
					}
				}
			}
		});
		tlvDataProcessingThread.start();
	}

	/**
	 * Method which formats the given input & converts the input into objects.
	 * Please note that this method checks for at least 3 bytes in the data &
	 * validates the same
	 * 
	 * @param tlvData
	 * @return
	 */
	public static List<TLVProcessedRawData> getProcessedRawData(
			String tlvRawData) {
		if (StringUtility.isNullOrEmpty(tlvRawData)) {
			return new ArrayList<>();
		}
		return getProcessedDataForPartOfData(tlvRawData);
	}

	private static List<TLVProcessedRawData> getProcessedDataForPartOfData(
			String tlvData) {
		String[] rawDataBuffer = tlvData.split(ROW_DATA_SEP);

		if (rawDataBuffer.length < 3) {
			return null;
		}

		List<TLVProcessedRawData> processedRawDatas = new ArrayList<TLVProcessedRawData>();
		int startIndex = 0;
		ProcessedRawDataInfo rawDataPart = getPartOfRawData(startIndex,
				rawDataBuffer);

		processedRawDatas.add(rawDataPart.getTlvProcessedRawData());
		Boolean isMoreDataAvailableToProcess = rawDataPart
				.isMoreDataToProcess();

		/*
		 * Checking for further availability of the data in the given
		 * string/input/data buffer
		 */
		while (isMoreDataAvailableToProcess) {
			startIndex += 2;
			ProcessedRawDataInfo continousData = getPartOfRawData(startIndex,
					rawDataBuffer);
			processedRawDatas.add(continousData.getTlvProcessedRawData());
			isMoreDataAvailableToProcess = continousData.isMoreDataToProcess();
		}
		return processedRawDatas;
	}

	private static ProcessedRawDataInfo getPartOfRawData(int startIndex,
			String[] tlvDataInfo) {
		TLVDataType dataType = TLVDataType
				.getTLVDataTypeByName(tlvDataInfo[startIndex]);

		int givenLength = Integer.valueOf(tlvDataInfo[startIndex + 1]);
		String dataPassed = tlvDataInfo[startIndex + 2];
		int dataOriginalLength = dataPassed.length();

		// Math.Min is handling the cases where given data length is greater
		// then the
		// length of given data
		String applicableData = dataPassed.substring(0,
				Math.min(givenLength, dataOriginalLength));
		String remainingData = dataPassed.substring(givenLength,
				dataPassed.length());

		boolean isMoreDataAvailableToProcess = StringUtility
				.isNullOrEmpty(remainingData) ? false : true;
		tlvDataInfo[startIndex + 2] = remainingData;
		return new ProcessedRawDataInfo(isMoreDataAvailableToProcess,
				new TLVProcessedRawData(dataType, givenLength, applicableData));
	}

	private static class ProcessedRawDataInfo {

		private boolean isMoreDataToProcess;
		private TLVProcessedRawData tlvProcessedRawData;

		public ProcessedRawDataInfo(boolean isMoreDataToProcess,
				TLVProcessedRawData tlvRawData) {
			setMoreDataToProcess(isMoreDataToProcess);
			setTlvProcessedRawData(tlvRawData);
		}

		public boolean isMoreDataToProcess() {
			return isMoreDataToProcess;
		}

		private void setMoreDataToProcess(boolean isMoreDataToProcess) {
			this.isMoreDataToProcess = isMoreDataToProcess;
		}

		public TLVProcessedRawData getTlvProcessedRawData() {
			return tlvProcessedRawData;
		}

		private void setTlvProcessedRawData(
				TLVProcessedRawData tlvProcessedRawData) {
			this.tlvProcessedRawData = tlvProcessedRawData;
		}

	}

}
