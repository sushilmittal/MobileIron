package com.mobileiron.managers;

import java.util.Scanner;

/**
 * 
 * @author Sushil Mittal
 *
 */
public class TLVProcessManager {

	public static void main(String[] args) {

		Scanner inputScanner = new Scanner(System.in);
		while (inputScanner.hasNext()) {
			String tlvData = inputScanner.next();
			TLVProcessor.addForProcessing(tlvData);
		}
		inputScanner.close();
	}
}
