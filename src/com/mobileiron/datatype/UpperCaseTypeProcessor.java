package com.mobileiron.datatype;

import com.mobileiron.data.TLVProcessedRawData;
import com.mobileiron.data.TLVProcessedData;

public class UpperCaseTypeProcessor extends AbstractTypeProcessor {

	public UpperCaseTypeProcessor(TLVProcessedRawData tlvData) {
		super(tlvData);
	}

	@Override
	public TLVProcessedData getProcessedData() {
		TLVProcessedRawData rowData = getTlvData();
		return new TLVProcessedData(rowData.getType(), rowData.getData()
				.toUpperCase());
	}
}
