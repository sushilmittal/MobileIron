package com.mobileiron.datatype;

import com.mobileiron.data.TLVProcessedRawData;
import com.mobileiron.data.TLVProcessedData;

public class InvalidTypeProcessor extends AbstractTypeProcessor {
	private static final String TYPE_NOT_VALID_MSG = "Type not valid";

	public InvalidTypeProcessor(TLVProcessedRawData tlvData) {
		super(tlvData);
	}

	@Override
	public TLVProcessedData getProcessedData() {
		return new TLVProcessedData(getTlvData().getType(), TYPE_NOT_VALID_MSG);
	}

}
