package com.mobileiron.datatype;

import com.mobileiron.data.TLVProcessedRawData;
import com.mobileiron.data.TLVProcessedData;

public abstract class AbstractTypeProcessor {
	private TLVProcessedRawData tlvData = null;

	AbstractTypeProcessor(TLVProcessedRawData tlvData) {
		this.tlvData = tlvData;
	}

	protected TLVProcessedRawData getTlvData() {
		return tlvData;
	}

	public abstract TLVProcessedData getProcessedData();

}
