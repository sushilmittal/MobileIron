package com.mobileiron.datatype;

import com.mobileiron.data.TLVProcessedRawData;
import com.mobileiron.data.TLVProcessedData;

public class ReplaceTypeProcessor extends AbstractTypeProcessor {
	private static final String REPLACE_MSG = "THIS STRING";

	public ReplaceTypeProcessor(TLVProcessedRawData tlvData) {
		super(tlvData);
	}

	@Override
	public TLVProcessedData getProcessedData() {
		TLVProcessedRawData rowData = getTlvData();
		return new TLVProcessedData(rowData.getType(), REPLACE_MSG);
	}

}
