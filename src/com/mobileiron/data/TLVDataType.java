package com.mobileiron.data;

import java.util.HashMap;
import java.util.Map;

import com.mobileiron.datatype.AbstractTypeProcessor;
import com.mobileiron.datatype.InvalidTypeProcessor;
import com.mobileiron.datatype.ReplaceTypeProcessor;
import com.mobileiron.datatype.UpperCaseTypeProcessor;

/**
 * 
 * @author Sushil Mittal
 *
 */
public enum TLVDataType {

	UPPER_CASE("UPPRCS") {

		@Override
		protected AbstractTypeProcessor getTLVDataTypeProcessor(
				TLVProcessedRawData tlvData) {
			return new UpperCaseTypeProcessor(tlvData);
		}
	},
	REPLACE("REPLCE") {

		@Override
		protected AbstractTypeProcessor getTLVDataTypeProcessor(
				TLVProcessedRawData tlvData) {
			return new ReplaceTypeProcessor(tlvData);
		}
	},

	// Others can contain anything eg.TAG,BAG etc so putting null in the name
	OTHERS(null) {

		@Override
		protected AbstractTypeProcessor getTLVDataTypeProcessor(
				TLVProcessedRawData tlvData) {
			return new InvalidTypeProcessor(tlvData);
		}

		@Override
		public String getFormattedOutput(TLVProcessedData processedData) {
			return processedData.getData();
		}
	};

	protected abstract AbstractTypeProcessor getTLVDataTypeProcessor(
			TLVProcessedRawData tlvData);

	public String getFormattedOutput(TLVProcessedData processedData) {
		return processedData.getDataType().getTlvDataName() + "-"
				+ processedData.getData();
	}

	private String tlvDataName;

	TLVDataType(String name) {
		this.tlvDataName = name;
	}

	public String getTlvDataName() {
		return tlvDataName;
	}

	public void setTlvDataName(String tlvDataName) {
		this.tlvDataName = tlvDataName;
	}

	public static TLVDataType getTLVDataTypeByName(String tlvTypeName) {
		TLVDataType tlvType = tlvNameToType.get(tlvTypeName);
		if (tlvType == null) {
			return TLVDataType.OTHERS;
		}
		return tlvType;
	}

	private static Map<String, TLVDataType> tlvNameToType = new HashMap<String, TLVDataType>();

	static {
		for (TLVDataType tlvType : TLVDataType.values()) {
			tlvNameToType.put(tlvType.getTlvDataName(), tlvType);
		}
	}

}
