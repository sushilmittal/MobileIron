package com.mobileiron.data;

public class TLVProcessedData {
	private TLVDataType dataType;
	private String data;

	public TLVProcessedData(TLVDataType dataType, String data) {
		super();
		setDataType(dataType);
		setData(data);
	}

	public String getFormattedData() {
		return this.getDataType().getFormattedOutput(this);
	}

	public TLVDataType getDataType() {
		return dataType;
	}

	private void setDataType(TLVDataType dataType) {
		this.dataType = dataType;
	}

	public String getData() {
		return data;
	}

	private void setData(String data) {
		this.data = data;
	}

}
