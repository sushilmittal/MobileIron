package com.mobileiron.data;

import com.mobileiron.datatype.AbstractTypeProcessor;

public class TLVProcessedRawData {
	private TLVDataType type;
	private int dataLength;
	private String data;

	public TLVProcessedRawData(TLVDataType type, int dataLength, String data) {
		super();
		setType(type);
		setDataLength(dataLength);
		setData(data);
	}

	public TLVProcessedData getProcessedData() {
		return getTLVDataTypeProcessor().getProcessedData();
	}

	public AbstractTypeProcessor getTLVDataTypeProcessor() {
		return this.getType().getTLVDataTypeProcessor(this);
	}

	public TLVDataType getType() {
		return type;
	}

	private void setType(TLVDataType type) {
		this.type = type;
	}

	public int getDataLength() {
		return dataLength;
	}

	private void setDataLength(int dataLength) {
		this.dataLength = dataLength;
	}

	public String getData() {
		return data;
	}

	private void setData(String data) {
		this.data = data;
	}

}
