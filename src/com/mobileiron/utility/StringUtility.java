package com.mobileiron.utility;

public class StringUtility {
	public static boolean isNullOrEmpty(String s) {
		return s == null || s.trim().equals("");
	}
}
