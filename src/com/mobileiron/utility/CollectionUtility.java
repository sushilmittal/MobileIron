package com.mobileiron.utility;

import java.util.Collection;

public class CollectionUtility {
	public static boolean isNullOrEmpty(Collection<? extends Object> collection) {
		return collection == null || collection.size() == 0;
	}
}
